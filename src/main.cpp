#include <iostream>
#include <fstream>
#include "controller.h"
#include "game.h"
#include "renderer.h"

int main() {
  constexpr std::size_t kFramesPerSecond{60};
  constexpr std::size_t kMsPerFrame{1000 / kFramesPerSecond};
  constexpr std::size_t kScreenWidth{640};
  constexpr std::size_t kScreenHeight{640};
  constexpr std::size_t kGridWidth{32};
  constexpr std::size_t kGridHeight{32};

  Renderer renderer(kScreenWidth, kScreenHeight, kGridWidth, kGridHeight);
  Controller controller;
  Game game(kGridWidth, kGridHeight);
  game.Run(controller, renderer, kMsPerFrame);
  std::cout << "Game has terminated successfully!\n";
  std::cout << "Score: " << game.GetScore() << "\n";
  std::cout << "Size: " << game.GetSize() << "\n";
  std::cout << "MaxSpeed: " << game.GetSpeed() << "\n";
  char name[50];
  std::cout << "Write your name: ";
  std::cin >> name;
  std::ofstream results("../results.txt", std::ios_base::app);
  results << "User: " << name << " -> Score: " << game.GetScore() << "ptos. -> Size: " << game.GetSize() << "ptos. -> MaxSpeed: " << game.GetSpeed() << "\n";
  results.close();
  return 0;
}