#include <iostream>
#include "game.h"
#include "SDL.h"
#include <random>

Game::Game(std::size_t grid_width, std::size_t grid_height)
    : snake(grid_width, grid_height),
      grid_width(grid_width),
      grid_height(grid_height),
      engine(dev()),
      random_w(0, static_cast<int>(grid_width - 1)),
      random_h(0, static_cast<int>(grid_height - 1)) {
        foods = std::make_unique<Food>(grid_width, grid_height);
        foods->genObjects();
}

void Game::Run(Controller const &controller, Renderer &renderer,
               std::size_t target_frame_duration) {
  Uint32 title_timestamp = SDL_GetTicks();
  Uint32 frame_start;
  Uint32 frame_end;
  Uint32 frame_duration;

  SDL_Point food;
  SDL_Point improve;
  std::vector<SDL_Point> imprPos{};
  std::string imprColour = "";
  int frame_count = 0;

  while (snake.alive) {
    frame_start = SDL_GetTicks();

    // Input, Update, Render - the main game loop.
    controller.HandleInput(snake.alive, snake);
    Update();

    foods->getPosition(food.x, food.y);
    if(!imprPos.empty())  imprPos.clear();
    if(!addition.empty()){
      for(auto impr : addition){
        impr->getPosition(improve.x, improve.y);
        imprColour = impr->getColour();
        imprPos.push_back(improve);
      }
    }
    renderer.Render(snake, food, foods->getColour(), imprPos, imprColour);

    frame_end = SDL_GetTicks();

    // Keep track of how long each loop through the input/update/render cycle
    // takes.
    frame_count++;
    frame_duration = frame_end - frame_start;

    // After every second, update the window title.
    if (frame_end - title_timestamp >= 1000) {
      renderer.UpdateWindowTitle(score, frame_count);
      frame_count = 0;
      title_timestamp = frame_end;
    }

    // If the time for this frame is too small (i.e. frame_duration is
    // smaller than the target ms_per_frame), delay the loop to
    // achieve the correct frame rate.
    if (frame_duration < target_frame_duration) {
      SDL_Delay(target_frame_duration - frame_duration);
    }
  }
}

void Game::Update() {
  if (!snake.alive) return;

  snake.Update();

  int new_x = static_cast<int>(snake.head_x);
  int new_y = static_cast<int>(snake.head_y);

  SDL_Point foodPos;
  SDL_Point imprPos;

  foods->getPosition(foodPos.x, foodPos.y);

  // Check if there's food over here
  if (foodPos.x == new_x && foodPos.y == new_y) {
    score += foods->getScore();
    foods->genObjects();
    // Grow snake and increase speed.
    snake.GrowBody();
    snake.speed += 0.02;
    foods->textFood();
  }

  if(!addition.empty()){
    for(auto impr : addition){
      impr->getPosition(imprPos.x, imprPos.y);
      if (imprPos.x == new_x && imprPos.y == new_y) {
        score += impr->getScore();
        snake.GrowBody();
        snake.speed += static_cast<float>(impr->getScore())/100.0;
        impr->textImproves();
        removeImprove(impr);
        break;
      }
    }
  }else if(score%5==0 && score>0){
    improvesGenerator();
  }
}

void Game::createImprove(int score){
  addition.emplace_back(std::make_shared<Improves>(grid_width, grid_height, score));
}

void Game::arrangeImproves() {
  SDL_Point improvesPosition;
  std::vector<SDL_Point> foodPos;
  foods->getPosition(improvesPosition.x, improvesPosition.y);
  foodPos.push_back(improvesPosition);

  for (auto impr : addition) {
    impr->genObjects();
    impr->getPosition(improvesPosition.x, improvesPosition.y);
    bool posConflict{true};

    while (conflicts(foodPos, improvesPosition.x, improvesPosition.y)) {
      impr->genObjects();
      impr->getPosition(improvesPosition.x, improvesPosition.y);
    }
    foodPos.push_back(improvesPosition);
  }
}

void Game::removeImprove(std::shared_ptr<Improves> imprs) {
  for (auto it = addition.begin(); it != addition.end();) {
    if (*it == imprs) {
      it = addition.erase(it);
    } else {
      it++;
    }
  }
}

void Game::improvesGenerator() {
  std::shared_ptr<MessageQueue<int>> queue(new MessageQueue<int>);
  std::vector<std::future<void>> futures;
  int numImproves = 3;

  for (int i = 1; i <= numImproves; i++) {
    int mess = i;
    futures.emplace_back(std::async(std::launch::async, &MessageQueue<int>::send, queue, std::move(mess)));
  }

  int impr = 0;
  std::random_device rd;
  std::default_random_engine engi(rd());
  std::uniform_int_distribution<int> distr(2,5);
  while (impr != numImproves) {
    createImprove(distr(engi));
    impr++;
  }

  arrangeImproves();

  std::for_each(futures.begin(), futures.end(), [](std::future<void> &ftr) {
    ftr.wait();
  });

}

bool Game::conflicts(std::vector<SDL_Point> position, const int x, const int y){
  for(auto pos : position){
    if(x==pos.x && y == pos.y){
      return true;
    }
  }
  return false;
}

int Game::GetScore() const {
  return score;
}

int Game::GetSize() const {
  return snake.size;
}

float Game::GetSpeed() const {
  return snake.speed;
}