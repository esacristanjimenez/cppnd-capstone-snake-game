#ifndef OBJECTS_H
#define OBJECTS_H

#include <memory>
#include <random>
#include "SDL.h"
#include "controller.h"
#include "renderer.h"
#include "snake.h"

class Object{
  public:
    Object(std::size_t grid_width, std::size_t grid_height);
    
    void genObjects();
    void getPosition(int &x, int &y);
    int getScore() const;
    std::string getColour() const;

  protected:
    SDL_Point position;
    int score{0};
    std::string colour;
    std::random_device dev;
    std::mt19937 engine;
    std::uniform_int_distribution<int> random_w;
    std::uniform_int_distribution<int> random_h;
};

class Food: public Object{
  public:
    Food(std::size_t grid_width, std::size_t grid_height);
    void textFood();
};

class Improves: public Object{
  public:
    Improves(std::size_t grid_width, std::size_t grid_height, int scoret);
    void textImproves();
};

#endif