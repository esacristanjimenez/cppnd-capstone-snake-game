#ifndef MESSAGE_QUEUE_H
#define MESSAGE_QUEUE_H

#include <iostream>
#include <thread>
#include <queue>
#include <future>
#include <mutex>
#include <algorithm>
#include "objects.h"

template <class T>
class MessageQueue {
  public:
    T receive()
    {
      std::unique_lock<std::mutex> uniqueLock(_mutex);
      _condition.wait(uniqueLock, [this] {return !_messages.empty(); });

      T message = std::move(_messages.back());
      _messages.pop_back();

      return message;
    }

    void send(T &&message)
    {
      std::lock_guard<std::mutex> lockGuard(_mutex);

      _messages.push_back(std::move(message));
      _condition.notify_one();
    }

  private:
    std::mutex _mutex;
    std::deque<T> _messages;
    std::condition_variable _condition;
};

#endif