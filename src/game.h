#ifndef GAME_H
#define GAME_H

#include <random>
#include <memory>
#include "message_queue.h"
#include "SDL.h"
#include "controller.h"
#include "renderer.h"
#include "snake.h"
#include "objects.h"

class Game {
 public:
  Game(std::size_t grid_width, std::size_t grid_height);
  void Run(Controller const &controller, Renderer &renderer,
           std::size_t target_frame_duration);
  
  int GetScore() const;
  int GetSize() const;
  float GetSpeed() const;
 
 private:
  Snake snake;
  
  std::unique_ptr<Food> foods;
  std::vector<std::shared_ptr<Improves>> addition;

  std::random_device dev;
  std::mt19937 engine;
  std::uniform_int_distribution<int> random_w;
  std::uniform_int_distribution<int> random_h;
  std::size_t grid_height;
  std::size_t grid_width;
  int score{0};

  void Update();
  void arrangeImproves();
  void createImprove(int score);
  void removeImprove(std::shared_ptr<Improves> impr);
  void improvesGenerator();
  bool conflicts(std::vector<SDL_Point> position, const int x, const int y);
};

#endif