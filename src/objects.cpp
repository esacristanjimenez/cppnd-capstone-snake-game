#include "objects.h"
#include <iostream>
#include "SDL.h"

Object::Object(std::size_t grid_width, std::size_t grid_height)
      : engine(dev()),
        random_w(0, static_cast<int>(grid_width - 1)),
        random_h(0, static_cast<int>(grid_height - 1)){
          colour="";
        }

void Object::genObjects(){
  int posX = random_w(engine);
  int posY = random_h(engine);
  position.x = posX;
  position.y = posY;
}

void Object::getPosition(int &x, int &y){
  x=position.x;
  y=position.y;
}

int Object::getScore() const{
  return score;
}

std::string Object::getColour() const{
  return colour;
}

//******************************************************************************************************
Food::Food(std::size_t grid_width, std::size_t grid_height) : Object(grid_width, grid_height){
  genObjects();
  score = 1;
  colour = "green";
}

void Food::textFood(){
  std::cout << "Food recolected 1 point" << std::endl;
}

//******************************************************************************************************
Improves::Improves(std::size_t grid_width, std::size_t grid_height, int scoret) : Object(grid_width, grid_height){
  genObjects();
  score = scoret;
  colour = "blue";
}

void Improves::textImproves(){
  std::cout << "Improves recolected " << score << " point additional" << std::endl;
}
