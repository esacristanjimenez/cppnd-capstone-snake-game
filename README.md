# CPPND: Capstone Snake Game Example

This is a starter repo for the Capstone project in the [Udacity C++ Nanodegree Program](https://www.udacity.com/course/c-plus-plus-nanodegree--nd213). The code for this repo was inspired by [this](https://codereview.stackexchange.com/questions/212296/snake-game-in-c-with-sdl) excellent StackOverflow post and set of responses.

<img src="snake_game_crated.gif"/>

The Capstone Project gives you a chance to integrate what you've learned throughout this program. This project will become an important part of your portfolio to share with current and future colleagues and employers.

In this project, you can build your own C++ application or extend this Snake game, following the principles you have learned throughout this Nanodegree Program. This project will demonstrate that you can independently create applications using a wide range of C++ features.

## Dependencies for Running Locally
* cmake >= 3.7
  * All OSes: [click here for installation instructions](https://cmake.org/install/)
* make >= 4.1 (Linux, Mac), 3.81 (Windows)
  * Linux: make is installed by default on most Linux distros
  * Mac: [install Xcode command line tools to get make](https://developer.apple.com/xcode/features/)
  * Windows: [Click here for installation instructions](http://gnuwin32.sourceforge.net/packages/make.htm)
* SDL2 >= 2.0
  * All installation instructions can be found [here](https://wiki.libsdl.org/Installation)
  >Note that for Linux, an `apt` or `apt-get` installation is preferred to building from source. 
* gcc/g++ >= 5.4
  * Linux: gcc / g++ is installed by default on most Linux distros
  * Mac: same deal as make - [install Xcode command line tools](https://developer.apple.com/xcode/features/)
  * Windows: recommend using [MinGW](http://www.mingw.org/)

## Basic Build Instructions

1. Clone this repo.
2. Make a build directory in the top level directory: `mkdir build && cd build`
3. Compile: `cmake .. && make`
4. Run it: `./SnakeGame`.

## New caracteristics
New version is created in this repository.

This version includes improvements that allow the user to increase the score and movement speed, without increasing the size of the snake.

The game starts normally. When the user collects 5 objects, 3 additional items appear on the screen that the user can pick up as upgrades. These upgrades provide additional score and speed to the user.

From this point on, each time the user achieves a score multiple of 5 and there are no upgrades on the screen, 3 new upgrades will appear.

Once the game is finished, either because the user has lost or closed the screen, the results can be saved in a text file (txt).

## Rubric Audacity

* Readme:
  * A README with instructions is included with the project
  * The README indicates the new features you added to the game.
  * The README includes information about each rubric point addressed.

* Compiling and Testing
  * The submission must compile and run without errors on the Udacity project workspace.

* Loops, Functions, I/O
  * The project demonstrates an understanding of C++ functions and control structures.
  * The project reads data from a file and process the data, or the program writes data to a file.
  * The project accepts user input and processes the input
  * The project uses data structures and immutable variables.

* Object Oriented Programming
  * One or more classes are added to the project with appropriate access specifiers for class members.
  * Class constructors utilize member initialization lists.
  * Classes follow an appropriate inheritance hierarchy with virtual and override funtions 
  * Templates generalize functions or classes in the project.

* Memory Management
  * The project makes use of references in function declarations.
  * The project uses scope / Resource Acquisition Is Initialization (RAII) where appropriate.
  * The project uses move semantics to move data instead of copying it, where possible.
  * The project uses smart pointers instead of raw pointers.

* Concurrency
  * The project uses multithreading.
  * A promise and future is used in the project.
  * A mutex or lock is used in the project.
  * A condition variable is used in the project.

## CC Attribution-ShareAlike 4.0 International


Shield: [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
